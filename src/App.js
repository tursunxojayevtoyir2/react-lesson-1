import logo from './logo.svg';
import './App.css';
import React from 'react';

class App extends React.Component {

  render() {
    return (
      <>
        <nav className="nav">
          <div>
          <h1>WIX</h1>
          <i class="fas fa-desktop"></i>
          <i class="fas fa-mobile-alt"></i>
          </div>
          <div>
          <p>Нажмите Редактировать, чтобы создать ваш сайт!</p>
          <p><a href='#'>Подробнее</a></p>
          <button className="btn btn-primary rounded-pill">Редактировать</button>
          </div>
        </nav>
        <section className="section-1">
          <div className="container">
            <div className="row">
              <div className="col">
              <p className='text'><i>Олег Марков</i></p>
              <ul className="list-ul">
                <li className="list-group">Главная</li>
                <li className="list-group">Обо мне</li>
                <li className="list-group">Связаться</li>
              </ul>
              </div>
            </div>
          </div>
        </section>
        <section className="section-2">
          <div className="container">
            <div className="row div-flex">
              <div className="col">
                <div className="img">
                  <img src={"/img/person.webp"} />
                </div>
                <div className="img">
                <img src={"/img/number.webp"} />
                </div>
                <div className="img">
                <img src={"/img/car.webp"} />
                </div>
              </div>
              <div className="col">
                <div className="img">
                <img src={"/img/veli.webp"} />
                </div>
                <div className="img">
                <img src={"/img/soat.webp"} />
                </div>
                <div className="img">
                <img src={"/img/cat.webp"} />
                </div>
              </div>
              <div className="col">
                <div className="img">
                <img src={"/img/radio.webp"} />
                </div>
                <div className="img">
                <img src={"/img/coyne.webp"} />
                </div>
                <div className="img">
                <img src={"/img/tarvuz.webp"} />
                </div>
              </div>
            </div>
          </div>
        </section>
        <footer>
          <div className="container">
            <div className="row">
              <div className="col">
                <p className="text-center">© 2023 Олег Марков. Сайт создан на Wix.com.</p>
                <div className="icon">
                <i class="fab fa-facebook-square"></i>
                <i class="fab fa-twitter-square"></i>
                <i class="fab fa-twitch"></i>
                <i class="fab fa-youtube-square"></i>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </>
    )
  }

}

export default App;
